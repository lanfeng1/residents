# Generated by Django 2.2.3 on 2020-05-21 13:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_auto_20200521_1203'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mobile_residents',
            name='phone',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='residents_information',
            name='phone',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='volunteers',
            name='phone',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
