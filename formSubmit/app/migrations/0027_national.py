# Generated by Django 2.2.3 on 2020-05-27 13:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0026_auto_20200524_2308'),
    ]

    operations = [
        migrations.CreateModel(
            name='national',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.TextField()),
            ],
            options={
                'verbose_name': '民族json',
                'verbose_name_plural': '民族json',
            },
        ),
    ]
