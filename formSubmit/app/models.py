from django.db import models

# Create your models here.
class residents_information(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True,verbose_name = "姓名")
    phone = models.CharField(max_length=20, blank=True, null=True,verbose_name = "手机号")
    age = models.CharField(max_length=20, blank=True, null=True,verbose_name = "年龄")
    idcard = models.CharField(max_length=20, blank=True, null=True,verbose_name = "身份证号码")
    car = models.CharField(max_length=128, blank=True, null=True,verbose_name = "车辆信息")
    gender = models.CharField(max_length=128, blank=True, null=True,verbose_name = "性别")
    crowd = models.CharField(max_length=128, blank=True, null=True,verbose_name = "是否特殊人群")
    special_populations = models.CharField(max_length=128, blank=True, null=True,verbose_name = "特殊人群选择")
    site = models.CharField(max_length=128, blank=True, null=True,verbose_name = "地址")
    face = models.CharField(max_length=128, blank=True, null=True,verbose_name = "政治面貌")
    relation = models.CharField(max_length=128, blank=True, null=True,verbose_name = "户主关系")
    units = models.TextField(max_length=128, blank=True, null=True,verbose_name = "单位")
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)

    class Meta:
        verbose_name = '常驻居民信息'
        verbose_name_plural = verbose_name


class mobile_residents(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True, verbose_name="姓名")
    phone = models.CharField(max_length=20, blank=True, null=True, verbose_name="手机号")
    age = models.CharField(max_length=20, blank=True, null=True, verbose_name="年龄")
    idcard = models.CharField(max_length=20, blank=True, null=True, verbose_name="身份证号码")
    gender = models.CharField(max_length=128, blank=True, null=True, verbose_name="性别")
    date = models.CharField(max_length=20, blank=True, null=True,verbose_name="入住时间")
    lease = models.CharField(max_length=128, blank=True, null=True,verbose_name="租期")
    site = models.CharField(max_length=128, blank=True, null=True,verbose_name="暂住地址")
    face = models.CharField(max_length=128, blank=True, null=True,verbose_name = "政治面貌")
    home = models.CharField(max_length=128, blank=True, null=True,verbose_name="户口所在")
    units = models.TextField(max_length=128, blank=True, null=True,verbose_name = "单位")
    username = models.CharField(max_length=128, blank=True, null=True,verbose_name="户主姓名")
    userphone = models.CharField(max_length=128, blank=True, null=True,verbose_name="户主手机号")
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)

    class Meta:
        verbose_name = '流动居民信息'
        verbose_name_plural = verbose_name

class volunteers(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True, verbose_name="姓名")
    phone = models.CharField(max_length=20, blank=True, null=True, verbose_name="手机号")
    age = models.CharField(max_length=20, blank=True, null=True, verbose_name="年龄")
    specialty = models.CharField(max_length=128, blank=True, null=True, verbose_name="特长")
    volunteer_time = models.CharField(max_length=128, blank=True, null=True, verbose_name="志愿时间")
    regional = models.CharField(max_length=128, blank=True, null=True, verbose_name="地域")
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)

    class Meta:
        verbose_name = '志愿者信息'
        verbose_name_plural = verbose_name