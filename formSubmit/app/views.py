from django.shortcuts import render
from django.forms.models import model_to_dict
from django.http.response import HttpResponse
import json
from .models import residents_information
from django.views.decorators.csrf import csrf_exempt

from django.conf import settings
from app.models import *

MEDIA_URL = settings.MEDIA_URL


@csrf_exempt
def permanent_form(request):
    if request.method == 'GET':
        context = {}
        data_list = residents_information.objects.filter()
        _d = []
        for i in data_list:
            _d.append(model_to_dict(i))
        context[''] = _d
        print(context)
        return HttpResponse(json.dumps(context), content_type="application/json")

    if request.method == 'POST':
        post_data = request.body.decode()
        print(post_data)
        _d = json.loads(post_data)
        # s = residents_information.objects.create()
        # s.save()
        # 
        # 
        # print('....', post_data)
        name = _d.get('name')
        phone = _d.get('phone')
        age = _d.get('age')
        idcard = _d.get('idcard')
        car = _d.get('car')
        crowd = _d.get('crowd')
        special_populations = _d.get('special_populations')
        site = _d.get('site')
        face = _d.get('face')
        relation = _d.get('relation')
        units = _d.get('units')
        gender = _d.get('gender')

        m = residents_information()

        m.name = name
        m.phone = phone
        m.age = age
        m.idcard = idcard
        m.car = car
        m.crowd = crowd
        m.gender = gender
        m.special_populations = special_populations
        m.site = site
        m.face = face
        m.relation = relation
        m.units = units
        m.save()
    return HttpResponse(json.dumps({'id': 'OK'}), content_type="application/json")


@csrf_exempt
def flow_form(request):
    if request.method == 'GET':
        context = {}
        data_list = mobile_residents.objects.filter()
        _d = []
        for i in data_list:
            _d.append(model_to_dict(i))
        context[''] = _d
        print(context)
        return HttpResponse(json.dumps(context), content_type="application/json")

    if request.method == 'POST':
        post_data = request.body.decode()
        print(post_data)
        _d = json.loads(post_data)
        # s = residents_information.objects.create()
        # s.save()
        #
        #
        # print('....', post_data)
        name = _d.get('name')
        phone = _d.get('phone')
        age = _d.get('age')
        idcard = _d.get('idcard')
        date = _d.get('date')
        lease = _d.get('lease')
        home = _d.get('home')
        site = _d.get('site')
        face = _d.get('face')
        relation = _d.get('relation')
        units = _d.get('units')
        gender = _d.get('gender')
        username = _d.get('username')
        userphone = _d.get('userphone')

        m = mobile_residents()

        m.name = name
        m.phone = phone
        m.age = age
        m.idcard = idcard
        m.date = date
        m.lease = lease
        m.gender = gender
        m.home = home
        m.site = site
        m.face = face
        m.relation = relation
        m.units = units
        m.username = username
        m.userphone = userphone
        m.save()
    return HttpResponse(json.dumps({'id': 'OK'}), content_type="application/json")


@csrf_exempt
def volunteers_form(request):
    if request.method == 'GET':
        context = {}
        data_list = volunteers.objects.filter()
        _d = []
        for i in data_list:
            _d.append(model_to_dict(i))
        context[''] = _d
        print(context)
        return HttpResponse(json.dumps(context), content_type="application/json")

    if request.method == 'POST':
        post_data = request.body.decode()
        print(post_data)
        _d = json.loads(post_data)
        # s = residents_information.objects.create()
        # s.save()
        #
        #
        # print('....', post_data)
        name = _d.get('name')
        phone = _d.get('phone')
        age = _d.get('age')
        volunteer_time = _d.get('volunteer_time')
        regional = _d.get('regional')
        specialty = _d.get('specialty')

        m = volunteers()

        m.name = name
        m.phone = phone
        m.age = age
        m.volunteer_time = volunteer_time
        m.regional = regional
        m.specialty = specialty
        m.save()
    return HttpResponse(json.dumps({'id': 'OK'}), content_type="application/json")
